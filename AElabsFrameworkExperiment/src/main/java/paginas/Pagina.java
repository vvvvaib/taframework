package paginas;

import com.gargoylesoftware.htmlunit.html.HtmlHeading1;
import com.gargoylesoftware.htmlunit.javascript.host.html.HTMLDivElement;
import webdriver.WebDriverFactory;

import com.google.common.base.Function;
import org.openqa.selenium.*;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;
import static com.google.common.collect.Lists.newArrayList;
import static java.text.MessageFormat.format;
import static org.junit.Assert.fail;
import static org.openqa.selenium.By.*;
import static org.openqa.selenium.support.PageFactory.initElements;

public abstract class Pagina<T extends Pagina<?>> {

    @FindBy(className = "notificationOK")
    private HTMLDivElement boodschapDiv;

    @FindBy(className = "notificationWarning")
    private HTMLDivElement waarschuwingDiv;

    protected WebDriver webDriver;

    protected final T myself;

    @FindBy(css = "#content h1")
    private HtmlHeading1 titel;

    public Pagina() {
        this(WebDriverFactory.getInstance().getWebDriver());
    }

    @SuppressWarnings("unchecked")
    public Pagina(WebDriver webDriver) {
        this.webDriver = webDriver;
        this.myself = (T) this;
        //filterAndLogWarningOrError();
        initElements(webDriver, this);
        // disableAsyncJqueryEffects();
        assertArrivedAt();
    }

    public static WebDriver switchToMainWindow() {
        return switchToWindow(0);
    }

    public static WebDriver switchToPopupWindow() {
        return switchToWindow(1);
    }

    private static WebDriver switchToWindow(int index) {
        WebDriver driver = WebDriverFactory.getInstance().getWebDriver();
        String handleOfNewWindow = newArrayList(driver.getWindowHandles()).get(index);
        return driver.switchTo().window(handleOfNewWindow);
    }

    public abstract boolean arrivedAt();

    protected String getPageUrl() {
        return webDriver.getCurrentUrl();
    }

    public T assertArrivedAt() {
        if (!this.arrivedAt()) {
//            String expectedUrl = getExpectedUrl();
            String nietOpPaginaBericht = format("-> url: ''{1}'' met titel ''{2}''",this.getPageUrl(), this.titel.getTextContent());
            fail(nietOpPaginaBericht);
        }
        return myself;
    }

    public T gaTerugViaBackButtonBrowser() {
        webDriver.navigate().back();
        return myself;
    }



    public Alert switchToAlert() {
        return webDriver.switchTo().alert();
    }

    protected WebElement findElement(String elementId) {
        try {
            return webDriver.findElement(id(elementId));
        } catch (NoSuchElementException notFoundOnPage) {
            return null;
        }
    }

    protected WebElement findElement(By by) {
        try {
            return webDriver.findElement(by);
        } catch (NoSuchElementException notFoundOnPage) {
            return null;
        }
    }

    protected List<WebElement> findElements(String name) {
        try {
            return webDriver.findElements(name(name));
        } catch (NoSuchElementException notFoundOnPage) {
            return null;
        }
    }
    protected List<WebElement> findElements(By by) {
        try {
            return webDriver.findElements(by);
        } catch (NoSuchElementException notFoundOnPage) {
            return null;
        }
    }

    protected WebElement findElementWithPartialLinkText(String partialLinkText) {
        try {
            return webDriver.findElement(partialLinkText(partialLinkText));
        } catch (NoSuchElementException notFoundOnPage) {
            return null;
        }
    }

    protected WebElement findElementWithXpath(String xpath) {
        try {
            return webDriver.findElement(xpath(xpath));
        } catch (NoSuchElementException notFoundOnPage) {
            return null;
        }
    }

    protected WebElement waitUntilElementPresent(By by, int timeout) {
        WebDriverWait wait = new WebDriverWait(webDriver, timeout);
        return wait.until(elementvisible(by));
    }

    protected WebElement waitUntilElementVisible(By by, int timeout) {
        WebDriverWait wait = new WebDriverWait(webDriver, timeout);
        return wait.until(elementvisible(by));
    }
    
    protected void waitUntilElementDisplayed(WebElement e, int timeout) {
		WebDriverWait wait = new WebDriverWait(WebDriverFactory.getInstance().getWebDriver(), timeout);
		wait.until(ExpectedConditions.visibilityOf(e));
		System.out.println("Present");
    }

    private Function<WebDriver, WebElement> elementvisible(final By locator) {
        return new Function<WebDriver, WebElement>() {
            @Override
            public WebElement apply(WebDriver driver) {
                if (driver.findElement(locator).isDisplayed()) {
                    return driver.findElement(locator);
                }
                return null;
            }
        };
    }

    protected WebElement waitUntilElementEnabled(By by, int timeout) {
        WebDriverWait wait = new WebDriverWait(webDriver, timeout);
        return wait.until(elementenabled(by));
    }

    private Function<WebDriver, WebElement> elementenabled(final By locator) {
        return new Function<WebDriver, WebElement>() {
            @Override
            public WebElement apply(WebDriver driver) {
                if (driver.findElement(locator).isEnabled()) {
                    return driver.findElement(locator);
                }
                return null;
            }
        };
    }
}