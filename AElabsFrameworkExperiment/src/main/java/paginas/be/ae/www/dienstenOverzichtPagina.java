/******************************************************************************
Developed by: Vaibhav
*******************************************************************************/


package paginas.be.ae.www;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import paginas.Pagina;

public class dienstenOverzichtPagina extends Pagina <dienstenOverzichtPagina> {

    @FindBy(id="page-title")
    private WebElement titelH1;

    @FindBy(xpath = "//*[@id=\"node-4\"]/div/div/div/div/p[2]/a")
            private WebElement qScanInfoAnch;

    @FindBy(xpath = "//*[@id=\"node-4\"]/div/div/div/div")
            private WebElement inhoudPaginaDiv;

    Logger logger = LoggerFactory.getLogger(dienstenOverzichtPagina.class);

    @Override
    public boolean arrivedAt() {
        waitUntilElementPresent(By.id("page-title"),50);
        return titelH1.getText().contains("Diensten");
    }

    public dienstenOverzichtPagina controleerInhoudVanPagina(){
        logger.info("We gaan de pagina checken op wat content");
        inhoudPaginaDiv.getText().contains("Audit");
        return this;
    }
    public qScanPagina klikOpMeerOverQScan(){
        logger.info("We klikken op de link 'Meer over Qscan");

            qScanInfoAnch.click();
        return new qScanPagina();



    }


}
