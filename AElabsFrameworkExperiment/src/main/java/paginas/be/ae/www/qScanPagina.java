/******************************************************************************
Developed by: Vaibhav
*******************************************************************************/

package paginas.be.ae.www;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import paginas.Pagina;
import java.util.List;

public class qScanPagina extends Pagina <qScanPagina> {

    @FindBy(id="page-title")
            private WebElement titelH1;

    @FindBy(xpath = "//*[@id=\"block-menu-block-1\"]/div/div/ul/li[3]/a")
            private WebElement jobsAnch;

    @FindBy(xpath = "//*[@id=\"node-6\"]/div/div/div/div")
            private WebElement inhoudDiv;

    Logger logger = LoggerFactory.getLogger(qScanPagina.class);

    @Override
    public boolean arrivedAt() {
        waitUntilElementPresent(By.id("page-title"),50);
        return titelH1.getText().contains("Qscan");
    }

    public qScanPagina ZoekInhoudOpPagina(String tekst){
        logger.info("We gaan opzoek naar het volgende stuk tekst: " + tekst);


            List<WebElement> elements = inhoudDiv.findElements(By.tagName("p"));
            for (WebElement element : elements) {
                if (element.getText().contains(tekst)) {
                   logger.info("De tekst werd gevonden");
                    return this;
                }
            }
        Assert.fail("De tekst werd niet gevonden");
        return this;
    }

    public jobsOverzichPagina klikOpJobs(){
        logger.info("Druk bovenaan in het menu op de tab 'jobs'");
            jobsAnch.click();
        return new jobsOverzichPagina();

    }
}
