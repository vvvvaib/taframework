/******************************************************************************
Developed by: Vaibhav
*******************************************************************************/

package paginas.be.ae.www;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import paginas.Pagina;



public class jobsOverzichPagina extends Pagina <jobsOverzichPagina> {

    @FindBy(id="page-title")
    private WebElement titelH1;

    @FindBy(xpath = "//*[@id=\"node-5\"]/div/div/div/div/p[8]/a")
            private WebElement infoficheSeniorTestAnalistAnch;

    Logger logger = LoggerFactory.getLogger(jobsOverzichPagina.class);

    @Override
    public boolean arrivedAt() {
        waitUntilElementPresent(By.id("page-title"),50);
        return titelH1.getText().contains("Jobs");
    }

    public infoFicheSeniorTestAnalistPagina klikOpInfoFicheSeniorTest(){
        logger.info("klik op de info fiche onder de titel 'Senior Test Analist'");
            infoficheSeniorTestAnalistAnch.click();
        return new infoFicheSeniorTestAnalistPagina();
    }
}
