/******************************************************************************
Developed by: Vaibhav
*******************************************************************************/

package paginas.be.ae.www;


import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import paginas.Pagina;



public class homePagina extends Pagina<homePagina> {

    @FindBy(id="blocks_col3")
    private WebElement nieuwsbriefDiv;

    @FindBy(xpath = "//*[@id=\"block-menu-block-1\"]/div/div/ul/li[1]/a")
    private WebElement dienstenAnch;

    @FindBy(xpath = "//*[@id=\"view-wrapper\"]/div[1]/div/div[6]")
    private WebElement trainingenAnch;

    Logger logger = LoggerFactory.getLogger(homePagina.class);

    @Override
    public boolean arrivedAt() {
     waitUntilElementPresent(By.id("blocks_col3"),50);
        return nieuwsbriefDiv.getText().contains("Nieuwsbrief");

    }

   public trainingenPagina klikOpTrainingenTab(){
       logger.info("Klik op de tab 'Trainingen'");
       trainingenAnch.click();
       return new trainingenPagina();
   }

    public dienstenOverzichtPagina klikOpDiensten(){
        logger.info("Klik bovenaan in het menu op 'Diensten");
        dienstenAnch.click();
        return new dienstenOverzichtPagina();
    }

}
