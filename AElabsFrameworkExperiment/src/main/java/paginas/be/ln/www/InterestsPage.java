/******************************************************************************
Developed by: Vaibhav
*******************************************************************************/

package paginas.be.ln.www;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import paginas.Pagina;

public class InterestsPage extends CommonControls {
	
    Logger logger = LoggerFactory.getLogger(InterestsPage.class);
    
    @Override
    public boolean arrivedAt() {
     waitUntilElementPresent(By.id("//*[@id=\"identity\"]/section/div/div/h3/a"),50);
        //return homePageIdentity.getText().contains("Aeqa Cap");
        return true;

    }
}
