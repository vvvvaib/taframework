/******************************************************************************
Developed by: Vaibhav
*******************************************************************************/

package paginas.be.ln.www;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class HomePage extends CommonControls{

    @FindBy(xpath = "//*[@id=\"identity\"]/section/div/div/h3/a")
    public WebElement homePageIdentity;
       
    Logger logger = LoggerFactory.getLogger(HomePage.class);
    
    @Override
    public boolean arrivedAt() {
    	waitUntilElementPresent(By.xpath("//*[@id=\"identity\"]/section/div/div/h3/a"),50);
        return homePageIdentity.getText().contains("Aeqa Cap");

    }
    
}
