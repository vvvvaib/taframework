/******************************************************************************
Developed by: Vaibhav
*******************************************************************************/

package paginas.be.ln.www;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import paginas.Pagina;

public class LoginPage extends Pagina<LoginPage> {

    @FindBy(xpath="//*[@id=\"login-email\"]")
    public WebElement emailID;

    @FindBy(xpath = "//*[@id=\"login-password\"]")
    public WebElement passwordField;

    @FindBy(xpath = ".//*[@value='Sign in']")
    public WebElement signInButton;

    Logger logger = LoggerFactory.getLogger(LoginPage.class);

    @Override
    public boolean arrivedAt() {
     //waitUntilElementPresent(By.xpath(".//*[text()='Sign in']"),50);
     waitUntilElementPresent(By.xpath(".//*[@value='Sign in']"),50);
        return true;

    }

   public HomePage klikOpLoginTab(){
       logger.info("Klik op de button 'Log in'");
       signInButton.click();
       return new HomePage();
   }
   
   public HomePage loginPositive(){
		new LoginPage().enteremailID("aeqacap@gmail.com")
		.enterpassword("Ae@1234")
		.klikOpLoginTab();
       return new HomePage();
   }
   
   public LoginPage enteremailID(String email){
       logger.info("Enter emailID");
       emailID.sendKeys(email);
       return this;
   }
   
   public LoginPage enterpassword(String password){
       logger.info("Enter password");
       passwordField.sendKeys(password);
       return this;
   }
   
  


}
