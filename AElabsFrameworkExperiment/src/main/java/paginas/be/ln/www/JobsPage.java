/******************************************************************************
Developed by: Vaibhav
*******************************************************************************/
package paginas.be.ln.www;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import paginas.Pagina;

public class JobsPage extends CommonControls {
	
    @FindBy(xpath = "//*[@id=\"jobs-home\"]/div[1]/h1")
    public WebElement jobsHomeText;
        
    Logger logger = LoggerFactory.getLogger(JobsPage.class);
    
    @Override
    public boolean arrivedAt() {
     waitUntilElementPresent(By.xpath("//*[@id=\"jobs-home\"]/div[1]/h1"),50);
        return jobsHomeText.getText().contains("Jobs");
      
    }
    

}
