/******************************************************************************
Developed by: Vaibhav
*******************************************************************************/

package paginas.be.ln.www;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class ProfilePage extends CommonControls {
	   
    @FindBy(xpath = "//*[@id=\"strength\"]/h3")
    public WebElement profileStrengthText;
    
    Logger logger = LoggerFactory.getLogger(ProfilePage.class);
    
    @Override
    public boolean arrivedAt() {
     waitUntilElementPresent(By.xpath("//*[@id=\"strength\"]/h3"),50);
        return profileStrengthText.getText().contains("Profile Strength");

    }

}
