/******************************************************************************
Developed by: Vaibhav
*******************************************************************************/

package paginas.be.ln.www;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SignOutPage extends CommonControls {

	@FindBy(xpath = "//*[@id=\"page-title\"]/h1")
	public WebElement youHaveSignedOutText;

	Logger logger = LoggerFactory.getLogger(SignOutPage.class);

	@Override
	public boolean arrivedAt() {
		waitUntilElementPresent(By.xpath("//*[@id=\"page-title\"]/h1"),50);
		return youHaveSignedOutText.getText().contains("You have signed out");
	}

}
