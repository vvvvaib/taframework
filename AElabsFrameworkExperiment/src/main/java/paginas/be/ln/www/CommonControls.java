/******************************************************************************
Developed by: Vaibhav
*******************************************************************************/

package paginas.be.ln.www;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import paginas.Pagina;

//import com.thoughtworks.selenium.webdriven.commands.Click;


public class CommonControls extends Pagina<CommonControls>{

    @FindBy(xpath = "//*[@id=\"main-site-nav\"]/ul/li[1]/a")
    public WebElement homeLink;
    
    @FindBy(xpath = "//*[@id=\"main-site-nav\"]/ul/li[2]/a")
    public WebElement profilesLink;
    

    @FindBy(xpath = "//*[@id=\"main-site-nav\"]/ul/li[3]/a")
    public WebElement myNetworkLink;
    
    
    @FindBy(xpath = "//*[@id=\"main-site-nav\"]/ul/li[4]/a")
    public WebElement jobsLink;
    
    @FindBy(xpath = "//*[@id=\"main-site-nav\"]/ul/li[5]/a")
    public WebElement interestsLink;
    
    
    @FindBy(xpath = "//*[@id=\"account-nav\"]/ul/li[4]/a")
    public WebElement accountsAndSettingsImage;
    
    @FindBy(xpath = "//*[@id=\"account-sub-nav\"]/div/div[2]/ul/li[1]/div/span/span[3]/a")
    public WebElement signOutElement;
    

    Logger logger = LoggerFactory.getLogger(CommonControls.class);
    
    @Override
    public boolean arrivedAt() {
    	//waitUntilElementPresent(By.xpath("//*[@id=\"main-site-nav\"]/ul/li[1]/a"),50);
    	waitUntilElementDisplayed(homeLink,50);
        return homeLink.getText().contains("Home");

    }
    
    public HomePage klikOpHomeLink(){
        logger.info("Klik op de link 'Home'");
        homeLink.click();
        return new HomePage();
    }
    
    public ProfilePage klikOpProfileLink(){
        logger.info("Klik op de link 'Profiles'");
        profilesLink.click();
        return new ProfilePage();
    }
    
    public MyNetworkPage klikOpmyNetworkLink(){
        logger.info("Klik op de link 'My Network'");
        myNetworkLink.click();
        return new MyNetworkPage();
    }
    
    public JobsPage klikOpJobsLink(){
        logger.info("Klik op de link 'Jobs'");
        jobsLink.click();
        return new JobsPage();
    }
    
    
    public InterestsPage klikOpInterestLink(){
        logger.info("Klik op de link 'Interests'");
        interestsLink.click();
        return new InterestsPage();
    }
    
    
    public HomePage hoverOpAccountsAndSettingsImage(){
        logger.info("hover Op Accounts And Settings Image");
        Actions action = new Actions(webDriver);
        action.moveToElement(accountsAndSettingsImage).build().perform();
        waitUntilElementPresent(By.xpath("//*[@id=\"account-sub-nav\"]/div/div[2]/ul/li[1]/div/span/span[3]/a"),50);
        return new HomePage();
    }
    
    public SignOutPage klikOpSignOut(){
        logger.info("signing out");
        new HomePage().hoverOpAccountsAndSettingsImage();
        signOutElement.click();
        return new SignOutPage();
    }
    
    
}
