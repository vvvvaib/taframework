package webdriver;

/** This class is used to set the resolution dimensions for emulating views of a web application on mobile devices
 *  Search for the device you need using ctrl+F to fetch the genre.
 *  IF the device of your choice is not present in this list,
 *  THEN add an additional "case" statement and include the correct dimensions.
 *  Devices are listed in alphabetic order by brand as primary filter:
 *  BRAND + DEVICE TYPE + VERSION (+) ROTATION
 * */


import org.junit.Assert;
import org.openqa.selenium.Dimension;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DimensionFactory {
    Logger logger = LoggerFactory.getLogger(DimensionFactory.class);
    public void fetchNewDimension(String genre) {
        String switchgenre = genre;
        Dimension d = null;

        switch (switchgenre) {
            case "Amazon Kindle Fire":
                d = new Dimension(600, 1024);
                logger.warn("EMULATED DISPLAY: '" + switchgenre + "' RESOLUTION (CSSPX):" + d.toString());
                WebDriverFactory.getInstance().getWebDriver().manage().window().setSize(d);
                WebDriverFactory.getInstance().getWebDriver().navigate().refresh();
                break;

            case "Amazon Kindle Fire landschape":
                d = new Dimension(1024, 600);
                logger.warn("EMULATED DISPLAY: '" + switchgenre + "' RESOLUTION (CSSPX):" + d.toString());
                WebDriverFactory.getInstance().getWebDriver().manage().window().setSize(d);
                WebDriverFactory.getInstance().getWebDriver().navigate().refresh();
                break;

            case "Amazon Kindle Fire HD7":
                d = new Dimension(480, 800);
                logger.warn("EMULATED DISPLAY: '" + switchgenre + "' RESOLUTION (CSSPX):" + d.toString());
                WebDriverFactory.getInstance().getWebDriver().manage().window().setSize(d);
                WebDriverFactory.getInstance().getWebDriver().navigate().refresh();
                break;

            case "Amazon Kindle Fire HD7 landscape":
                d = new Dimension(800, 480);
                logger.warn("EMULATED DISPLAY: '" + switchgenre + "' RESOLUTION (CSSPX):" + d.toString());
                WebDriverFactory.getInstance().getWebDriver().manage().window().setSize(d);
                WebDriverFactory.getInstance().getWebDriver().navigate().refresh();
                break;

            case "Amazon Kindle Fire HD8.9":
                d = new Dimension(800, 1280);
                logger.warn("EMULATED DISPLAY: '" + switchgenre + "' RESOLUTION (CSSPX):" + d.toString());
                WebDriverFactory.getInstance().getWebDriver().manage().window().setSize(d);
                WebDriverFactory.getInstance().getWebDriver().navigate().refresh();
                break;

            case "Amazon Kindle Fire HD8.9 landscape":
                d = new Dimension(1280, 800);
                logger.warn("EMULATED DISPLAY: '" + switchgenre + "' RESOLUTION (CSSPX):" + d.toString());
                WebDriverFactory.getInstance().getWebDriver().manage().window().setSize(d);
                WebDriverFactory.getInstance().getWebDriver().navigate().refresh();
                break;

            case "Apple IPad family":
                // Apple Ipad 1,2,3,4,Air & Mini, 2, 3
                d = new Dimension(768, 1024);
                logger.warn("EMULATED DISPLAY: '" + switchgenre + "' RESOLUTION (CSSPX):" + d.toString());
                WebDriverFactory.getInstance().getWebDriver().manage().window().setSize(d);
                WebDriverFactory.getInstance().getWebDriver().navigate().refresh();
                break;

            case "Apple IPad family landscape":
                // Apple Ipad 1,2,3,4,Air & Mini, 2, 3
                d = new Dimension(1024, 768);
                logger.warn("EMULATED DISPLAY: '" + switchgenre + "' RESOLUTION (CSSPX):" + d.toString());
                WebDriverFactory.getInstance().getWebDriver().manage().window().setSize(d);
                WebDriverFactory.getInstance().getWebDriver().navigate().refresh();
                break;

            case "Apple IPad Pro":
                d = new Dimension(1024, 1366);
                logger.warn("EMULATED DISPLAY: '" + switchgenre + "' RESOLUTION (CSSPX):" + d.toString());
                WebDriverFactory.getInstance().getWebDriver().manage().window().setSize(d);
                WebDriverFactory.getInstance().getWebDriver().navigate().refresh();
                break;

            case "Apple IPad Pro landscape":
                d = new Dimension(1366, 1024);
                logger.warn("EMULATED DISPLAY: '" + switchgenre + "' RESOLUTION (CSSPX):" + d.toString());
                WebDriverFactory.getInstance().getWebDriver().manage().window().setSize(d);
                WebDriverFactory.getInstance().getWebDriver().navigate().refresh();
                break;

            case "Apple IPhone 3,4":
                d = new Dimension(320, 480);
                logger.warn("EMULATED DISPLAY: '" + switchgenre + "' RESOLUTION (CSSPX):" + d.toString());
                WebDriverFactory.getInstance().getWebDriver().manage().window().setSize(d);
                WebDriverFactory.getInstance().getWebDriver().navigate().refresh();
                break;

            case "Apple IPhone 3,4 landscape":
                d = new Dimension(480, 320);
                logger.warn("EMULATED DISPLAY: '" + switchgenre + "' RESOLUTION (CSSPX):" + d.toString());
                WebDriverFactory.getInstance().getWebDriver().manage().window().setSize(d);
                WebDriverFactory.getInstance().getWebDriver().navigate().refresh();
                break;

            case "Apple IPhone 5":
                d = new Dimension(320, 568);
                logger.warn("EMULATED DISPLAY: '" + switchgenre + "' RESOLUTION (CSSPX):" + d.toString());
                WebDriverFactory.getInstance().getWebDriver().manage().window().setSize(d);
                WebDriverFactory.getInstance().getWebDriver().navigate().refresh();
                break;

            case "Apple IPhone 5 landscape":
                d = new Dimension(568, 320);
                logger.warn("EMULATED DISPLAY: '" + switchgenre + "' RESOLUTION (CSSPX):" + d.toString());
                WebDriverFactory.getInstance().getWebDriver().manage().window().setSize(d);
                WebDriverFactory.getInstance().getWebDriver().navigate().refresh();
                break;

            case "Apple IPhone 6":
                d = new Dimension(375, 667);
                logger.warn("EMULATED DISPLAY: '" + switchgenre + "' RESOLUTION (CSSPX):" + d.toString());
                WebDriverFactory.getInstance().getWebDriver().manage().window().setSize(d);
                WebDriverFactory.getInstance().getWebDriver().navigate().refresh();
                break;

            case "Apple IPhone 6 landscape":
                d = new Dimension(667, 375);
                logger.warn("EMULATED DISPLAY: '" + switchgenre + "' RESOLUTION (CSSPX):" + d.toString());
                WebDriverFactory.getInstance().getWebDriver().manage().window().setSize(d);
                WebDriverFactory.getInstance().getWebDriver().navigate().refresh();
                break;

            case "Apple IPhone 6 Plus":
                d = new Dimension(414, 736);
                logger.warn("EMULATED DISPLAY: '" + switchgenre + "' RESOLUTION (CSSPX):" + d.toString());
                WebDriverFactory.getInstance().getWebDriver().manage().window().setSize(d);
                WebDriverFactory.getInstance().getWebDriver().navigate().refresh();
                break;

            case "Apple IPhone 6 Plus landscape":
                d = new Dimension(736, 414);
                logger.warn("EMULATED DISPLAY: '" + switchgenre + "' RESOLUTION (CSSPX):" + d.toString());
                WebDriverFactory.getInstance().getWebDriver().manage().window().setSize(d);
                WebDriverFactory.getInstance().getWebDriver().navigate().refresh();
                break;

            case "Apple IPod Touch":
                d = new Dimension(320, 568);
                logger.warn("EMULATED DISPLAY: '" + switchgenre + "' RESOLUTION (CSSPX):" + d.toString());
                WebDriverFactory.getInstance().getWebDriver().manage().window().setSize(d);
                WebDriverFactory.getInstance().getWebDriver().navigate().refresh();
                break;

            case "Apple IPod Touch landscape":
                d = new Dimension(568, 320);
                logger.warn("EMULATED DISPLAY: '" + switchgenre + "' RESOLUTION (CSSPX):" + d.toString());
                WebDriverFactory.getInstance().getWebDriver().manage().window().setSize(d);
                WebDriverFactory.getInstance().getWebDriver().navigate().refresh();
                break;

            case "Asus Nexus 7v1":
                d = new Dimension(604, 966);
                logger.warn("EMULATED DISPLAY: '" + switchgenre + "' RESOLUTION (CSSPX):" + d.toString());
                WebDriverFactory.getInstance().getWebDriver().manage().window().setSize(d);
                WebDriverFactory.getInstance().getWebDriver().navigate().refresh();
                break;

            case "Asus Nexus 7v1 landscape":
                d = new Dimension(966, 604);
                logger.warn("EMULATED DISPLAY: '" + switchgenre + "' RESOLUTION (CSSPX):" + d.toString());
                WebDriverFactory.getInstance().getWebDriver().manage().window().setSize(d);
                WebDriverFactory.getInstance().getWebDriver().navigate().refresh();
                break;

            case "Asus Nexus 7v2":
                d = new Dimension(600, 960);
                logger.warn("EMULATED DISPLAY: '" + switchgenre + "' RESOLUTION (CSSPX):" + d.toString());
                WebDriverFactory.getInstance().getWebDriver().manage().window().setSize(d);
                WebDriverFactory.getInstance().getWebDriver().navigate().refresh();
                break;

            case "Asus Nexus 7v2 landscape":
                d = new Dimension(960, 600);
                logger.warn("EMULATED DISPLAY: '" + switchgenre + "' RESOLUTION (CSSPX):" + d.toString());
                WebDriverFactory.getInstance().getWebDriver().manage().window().setSize(d);
                WebDriverFactory.getInstance().getWebDriver().navigate().refresh();
                break;

            case "Blackberry Classic":
                //landscape or portrait angle does not affect any dimensions for this device
                d = new Dimension(390, 390);
                logger.warn("EMULATED DISPLAY: '" + switchgenre + "' RESOLUTION (CSSPX):" + d.toString());
                WebDriverFactory.getInstance().getWebDriver().manage().window().setSize(d);
                WebDriverFactory.getInstance().getWebDriver().navigate().refresh();
                break;

            case "Blackberry Leap":
                d = new Dimension(390, 695);
                logger.warn("EMULATED DISPLAY: '" + switchgenre + "' RESOLUTION (CSSPX):" + d.toString());
                WebDriverFactory.getInstance().getWebDriver().manage().window().setSize(d);
                WebDriverFactory.getInstance().getWebDriver().navigate().refresh();
                break;

            case "Blackberry Leap landscape":
                d = new Dimension(695, 390);
                logger.warn("EMULATED DISPLAY: '" + switchgenre + "' RESOLUTION (CSSPX):" + d.toString());
                WebDriverFactory.getInstance().getWebDriver().manage().window().setSize(d);
                WebDriverFactory.getInstance().getWebDriver().navigate().refresh();
                break;

            case "Blackberry Passport":
                //landscape or portrait angle does not affect any dimensions for this device
                d = new Dimension(504, 504);
                logger.warn("EMULATED DISPLAY: '" + switchgenre + "' RESOLUTION (CSSPX):" + d.toString());
                WebDriverFactory.getInstance().getWebDriver().manage().window().setSize(d);
                WebDriverFactory.getInstance().getWebDriver().navigate().refresh();
                break;

            case "Blackberry Playbook":
                d = new Dimension(600, 1024);
                logger.warn("EMULATED DISPLAY: '" + switchgenre + "' RESOLUTION (CSSPX):" + d.toString());
                WebDriverFactory.getInstance().getWebDriver().manage().window().setSize(d);
                WebDriverFactory.getInstance().getWebDriver().navigate().refresh();
                break;

            case "Blackberry Playbook landscape":
                d = new Dimension(1024, 600);
                logger.warn("EMULATED DISPLAY: '" + switchgenre + "' RESOLUTION (CSSPX):" + d.toString());
                WebDriverFactory.getInstance().getWebDriver().manage().window().setSize(d);
                WebDriverFactory.getInstance().getWebDriver().navigate().refresh();
                break;

            case "Blackberry Q10":
                //landscape or portrait angle does not affect any dimensions for this device
                d = new Dimension(346, 346);
                logger.warn("EMULATED DISPLAY: '" + switchgenre + "' RESOLUTION (CSSPX):" + d.toString());
                WebDriverFactory.getInstance().getWebDriver().manage().window().setSize(d);
                WebDriverFactory.getInstance().getWebDriver().navigate().refresh();
                break;

            case "Blackberry Torch 9800":
                d = new Dimension(360, 480);
                logger.warn("EMULATED DISPLAY: '" + switchgenre + "' RESOLUTION (CSSPX):" + d.toString());
                WebDriverFactory.getInstance().getWebDriver().manage().window().setSize(d);
                WebDriverFactory.getInstance().getWebDriver().navigate().refresh();
                break;

            case "Blackberry Torch 9800 landscape":
                d = new Dimension(480, 360);
                logger.warn("EMULATED DISPLAY: '" + switchgenre + "' RESOLUTION (CSSPX):" + d.toString());
                WebDriverFactory.getInstance().getWebDriver().manage().window().setSize(d);
                WebDriverFactory.getInstance().getWebDriver().navigate().refresh();
                break;

            case "Blackberry Z10":
                d = new Dimension(384, 640);
                logger.warn("EMULATED DISPLAY: '" + switchgenre + "' RESOLUTION (CSSPX):" + d.toString());
                WebDriverFactory.getInstance().getWebDriver().manage().window().setSize(d);
                WebDriverFactory.getInstance().getWebDriver().navigate().refresh();
                break;

            case "Blackberry Z10 portrait":
                d = new Dimension(640, 384);
                logger.warn("EMULATED DISPLAY: '" + switchgenre + "' RESOLUTION (CSSPX):" + d.toString());
                WebDriverFactory.getInstance().getWebDriver().manage().window().setSize(d);
                WebDriverFactory.getInstance().getWebDriver().navigate().refresh();
                break;

            case "Blackberry Z30":
                d = new Dimension(360, 640);
                logger.warn("EMULATED DISPLAY: '" + switchgenre + "' RESOLUTION (CSSPX):" + d.toString());
                WebDriverFactory.getInstance().getWebDriver().manage().window().setSize(d);
                WebDriverFactory.getInstance().getWebDriver().navigate().refresh();
                break;

            case "Blackberry Z30 landscape":
                d = new Dimension(640, 360);
                logger.warn("EMULATED DISPLAY: '" + switchgenre + "' RESOLUTION (CSSPX):" + d.toString());
                WebDriverFactory.getInstance().getWebDriver().manage().window().setSize(d);
                WebDriverFactory.getInstance().getWebDriver().navigate().refresh();
                break;

            case "Google Glass":
                // Single Dimension Device
                d = new Dimension(427, 240);
                logger.warn("EMULATED DISPLAY: '" + switchgenre + "' RESOLUTION (CSSPX):" + d.toString());
                WebDriverFactory.getInstance().getWebDriver().manage().window().setSize(d);
                WebDriverFactory.getInstance().getWebDriver().navigate().refresh();
                break;

            case "HTC 8X":
                d = new Dimension(320, 480);
                logger.warn("EMULATED DISPLAY: '" + switchgenre + "' RESOLUTION (CSSPX):" + d.toString());
                WebDriverFactory.getInstance().getWebDriver().manage().window().setSize(d);
                WebDriverFactory.getInstance().getWebDriver().navigate().refresh();
                break;

            case "HTC 8X landscape":
                d = new Dimension(480, 320);
                logger.warn("EMULATED DISPLAY: '" + switchgenre + "' RESOLUTION (CSSPX):" + d.toString());
                WebDriverFactory.getInstance().getWebDriver().manage().window().setSize(d);
                WebDriverFactory.getInstance().getWebDriver().navigate().refresh();
                break;

            case "HTC EVO 3D":
                d = new Dimension(360, 640);
                logger.warn("EMULATED DISPLAY: '" + switchgenre + "' RESOLUTION (CSSPX):" + d.toString());
                WebDriverFactory.getInstance().getWebDriver().manage().window().setSize(d);
                WebDriverFactory.getInstance().getWebDriver().navigate().refresh();
                break;

            case "HTC EVO 3D landscape":
                d = new Dimension(640, 360);
                logger.warn("EMULATED DISPLAY: '" + switchgenre + "' RESOLUTION (CSSPX):" + d.toString());
                WebDriverFactory.getInstance().getWebDriver().manage().window().setSize(d);
                WebDriverFactory.getInstance().getWebDriver().navigate().refresh();
                break;

            case "HTC Nexus 9":
                d = new Dimension(768, 1024);
                logger.warn("EMULATED DISPLAY: '" + switchgenre + "' RESOLUTION (CSSPX):" + d.toString());
                WebDriverFactory.getInstance().getWebDriver().manage().window().setSize(d);
                WebDriverFactory.getInstance().getWebDriver().navigate().refresh();
                break;

            case "HTC Nexus 9 landscape":
                d = new Dimension(1024, 768);
                logger.warn("EMULATED DISPLAY: '" + switchgenre + "' RESOLUTION (CSSPX):" + d.toString());
                WebDriverFactory.getInstance().getWebDriver().manage().window().setSize(d);
                WebDriverFactory.getInstance().getWebDriver().navigate().refresh();
                break;

            case "HTC One":
                d = new Dimension(360, 640);
                logger.warn("EMULATED DISPLAY: '" + switchgenre + "' RESOLUTION (CSSPX):" + d.toString());
                WebDriverFactory.getInstance().getWebDriver().manage().window().setSize(d);
                WebDriverFactory.getInstance().getWebDriver().navigate().refresh();
                break;

            case "HTC One landscape":
                d = new Dimension(640, 360);
                logger.warn("EMULATED DISPLAY: '" + switchgenre + "' RESOLUTION (CSSPX):" + d.toString());
                WebDriverFactory.getInstance().getWebDriver().manage().window().setSize(d);
                WebDriverFactory.getInstance().getWebDriver().navigate().refresh();
                break;

            case "Lenovo K900":
                d = new Dimension(360, 640);
                logger.warn("EMULATED DISPLAY: '" + switchgenre + "' RESOLUTION (CSSPX):" + d.toString());
                WebDriverFactory.getInstance().getWebDriver().manage().window().setSize(d);
                WebDriverFactory.getInstance().getWebDriver().navigate().refresh();
                break;

            case "Lenovo K900 landscape":
                d = new Dimension(640, 360);
                logger.warn("EMULATED DISPLAY: '" + switchgenre + "' RESOLUTION (CSSPX):" + d.toString());
                WebDriverFactory.getInstance().getWebDriver().manage().window().setSize(d);
                WebDriverFactory.getInstance().getWebDriver().navigate().refresh();
                break;

            case "LG G Pad 8.3":
                d = new Dimension(600, 960);
                logger.warn("EMULATED DISPLAY: '" + switchgenre + "' RESOLUTION (CSSPX):" + d.toString());
                WebDriverFactory.getInstance().getWebDriver().manage().window().setSize(d);
                WebDriverFactory.getInstance().getWebDriver().navigate().refresh();
                break;

            case "LG G Pad 8.3 landscape":
                d = new Dimension(960, 600);
                logger.warn("EMULATED DISPLAY: '" + switchgenre + "' RESOLUTION (CSSPX):" + d.toString());
                WebDriverFactory.getInstance().getWebDriver().manage().window().setSize(d);
                WebDriverFactory.getInstance().getWebDriver().navigate().refresh();
                break;

            case "LG G3,4":
                d = new Dimension(360, 640);
                logger.warn("EMULATED DISPLAY: '" + switchgenre + "' RESOLUTION (CSSPX):" + d.toString());
                WebDriverFactory.getInstance().getWebDriver().manage().window().setSize(d);
                WebDriverFactory.getInstance().getWebDriver().navigate().refresh();
                break;

            case "LG G3,4 landscape":
                d = new Dimension(640, 360);
                logger.warn("EMULATED DISPLAY: '" + switchgenre + "' RESOLUTION (CSSPX):" + d.toString());
                WebDriverFactory.getInstance().getWebDriver().manage().window().setSize(d);
                WebDriverFactory.getInstance().getWebDriver().navigate().refresh();
                break;

            case "LG Nexus 4":
                d = new Dimension(384, 640);
                logger.warn("EMULATED DISPLAY: '" + switchgenre + "' RESOLUTION (CSSPX):" + d.toString());
                WebDriverFactory.getInstance().getWebDriver().manage().window().setSize(d);
                WebDriverFactory.getInstance().getWebDriver().navigate().refresh();
                break;

            case "LG Nexus 4 landscape":
                d = new Dimension(640, 384);
                logger.warn("EMULATED DISPLAY: '" + switchgenre + "' RESOLUTION (CSSPX):" + d.toString());
                WebDriverFactory.getInstance().getWebDriver().manage().window().setSize(d);
                WebDriverFactory.getInstance().getWebDriver().navigate().refresh();
                break;

            case "LG Nexus 5":
                d = new Dimension(360, 640);
                logger.warn("EMULATED DISPLAY: '" + switchgenre + "' RESOLUTION (CSSPX):" + d.toString());
                WebDriverFactory.getInstance().getWebDriver().manage().window().setSize(d);
                WebDriverFactory.getInstance().getWebDriver().navigate().refresh();
                break;

            case "LG Nexus 5 landscape":
                d = new Dimension(640, 360);
                logger.warn("EMULATED DISPLAY: '" + switchgenre + "' RESOLUTION (CSSPX):" + d.toString());
                WebDriverFactory.getInstance().getWebDriver().manage().window().setSize(d);
                WebDriverFactory.getInstance().getWebDriver().navigate().refresh();
                break;

            case "LG Optimus G":
                d = new Dimension(384, 640);
                logger.warn("EMULATED DISPLAY: '" + switchgenre + "' RESOLUTION (CSSPX):" + d.toString());
                WebDriverFactory.getInstance().getWebDriver().manage().window().setSize(d);
                WebDriverFactory.getInstance().getWebDriver().navigate().refresh();
                break;

            case "LG Optimus G landscape":
                d = new Dimension(640, 384);
                logger.warn("EMULATED DISPLAY: '" + switchgenre + "' RESOLUTION (CSSPX):" + d.toString());
                WebDriverFactory.getInstance().getWebDriver().manage().window().setSize(d);
                WebDriverFactory.getInstance().getWebDriver().navigate().refresh();
                break;

            case "Microsoft Lumia 1020":
                d = new Dimension(320, 480);
                logger.warn("EMULATED DISPLAY: '" + switchgenre + "' RESOLUTION (CSSPX):" + d.toString());
                WebDriverFactory.getInstance().getWebDriver().manage().window().setSize(d);
                WebDriverFactory.getInstance().getWebDriver().navigate().refresh();
                break;

            case "Microsoft Lumia 1020 landscape":
                d = new Dimension(480, 320);
                logger.warn("EMULATED DISPLAY: '" + switchgenre + "' RESOLUTION (CSSPX):" + d.toString());
                WebDriverFactory.getInstance().getWebDriver().manage().window().setSize(d);
                WebDriverFactory.getInstance().getWebDriver().navigate().refresh();
                break;

            case "Microsoft Lumia 1520":
                d = new Dimension(432, 768);
                logger.warn("EMULATED DISPLAY: '" + switchgenre + "' RESOLUTION (CSSPX):" + d.toString());
                WebDriverFactory.getInstance().getWebDriver().manage().window().setSize(d);
                WebDriverFactory.getInstance().getWebDriver().navigate().refresh();
                break;

            case "Microsoft Lumia 1520 landscape":
                d = new Dimension(768, 432);
                logger.warn("EMULATED DISPLAY: '" + switchgenre + "' RESOLUTION (CSSPX):" + d.toString());
                WebDriverFactory.getInstance().getWebDriver().manage().window().setSize(d);
                WebDriverFactory.getInstance().getWebDriver().navigate().refresh();
                break;

            case "Microsoft Lumia 620":
                d = new Dimension(320, 480);
                logger.warn("EMULATED DISPLAY: '" + switchgenre + "' RESOLUTION (CSSPX):" + d.toString());
                WebDriverFactory.getInstance().getWebDriver().manage().window().setSize(d);
                WebDriverFactory.getInstance().getWebDriver().navigate().refresh();
                break;

            case "Microsoft Lumia 620 landscape":
                d = new Dimension(480, 320);
                logger.warn("EMULATED DISPLAY: '" + switchgenre + "' RESOLUTION (CSSPX):" + d.toString());
                WebDriverFactory.getInstance().getWebDriver().manage().window().setSize(d);
                WebDriverFactory.getInstance().getWebDriver().navigate().refresh();
                break;

            case "Microsoft Lumia 830":
                d = new Dimension(320, 480);
                logger.warn("EMULATED DISPLAY: '" + switchgenre + "' RESOLUTION (CSSPX):" + d.toString());
                WebDriverFactory.getInstance().getWebDriver().manage().window().setSize(d);
                WebDriverFactory.getInstance().getWebDriver().navigate().refresh();
                break;

            case "Microsoft Lumia 830 landscape":
                d = new Dimension(480, 320);
                logger.warn("EMULATED DISPLAY: '" + switchgenre + "' RESOLUTION (CSSPX):" + d.toString());
                WebDriverFactory.getInstance().getWebDriver().manage().window().setSize(d);
                WebDriverFactory.getInstance().getWebDriver().navigate().refresh();
                break;

            case "Microsoft Lumia 900":
                d = new Dimension(320, 480);
                logger.warn("EMULATED DISPLAY: '" + switchgenre + "' RESOLUTION (CSSPX):" + d.toString());
                WebDriverFactory.getInstance().getWebDriver().manage().window().setSize(d);
                WebDriverFactory.getInstance().getWebDriver().navigate().refresh();
                break;

            case "Microsoft Lumia 900 landscape":
                d = new Dimension(480, 320);
                logger.warn("EMULATED DISPLAY: '" + switchgenre + "' RESOLUTION (CSSPX):" + d.toString());
                WebDriverFactory.getInstance().getWebDriver().manage().window().setSize(d);
                WebDriverFactory.getInstance().getWebDriver().navigate().refresh();
                break;

            case "Microsoft Lumia 920":
                d = new Dimension(320, 480);
                logger.warn("EMULATED DISPLAY: '" + switchgenre + "' RESOLUTION (CSSPX):" + d.toString());
                WebDriverFactory.getInstance().getWebDriver().manage().window().setSize(d);
                WebDriverFactory.getInstance().getWebDriver().navigate().refresh();
                break;

            case "Microsoft Lumia 920 landscape":
                d = new Dimension(480, 320);
                logger.warn("EMULATED DISPLAY: '" + switchgenre + "' RESOLUTION (CSSPX):" + d.toString());
                WebDriverFactory.getInstance().getWebDriver().manage().window().setSize(d);
                WebDriverFactory.getInstance().getWebDriver().navigate().refresh();
                break;

            case "Microsoft Lumia 925":
                d = new Dimension(320, 480);
                logger.warn("EMULATED DISPLAY: '" + switchgenre + "' RESOLUTION (CSSPX):" + d.toString());
                WebDriverFactory.getInstance().getWebDriver().manage().window().setSize(d);
                WebDriverFactory.getInstance().getWebDriver().navigate().refresh();
                break;

            case "Microsoft Lumia 925 landscape":
                d = new Dimension(480, 320);
                logger.warn("EMULATED DISPLAY: '" + switchgenre + "' RESOLUTION (CSSPX):" + d.toString());
                WebDriverFactory.getInstance().getWebDriver().manage().window().setSize(d);
                WebDriverFactory.getInstance().getWebDriver().navigate().refresh();
                break;

            case "Microsoft Surface":
                d = new Dimension(768, 1366);
                logger.warn("EMULATED DISPLAY: '" + switchgenre + "' RESOLUTION (CSSPX):" + d.toString());
                WebDriverFactory.getInstance().getWebDriver().manage().window().setSize(d);
                WebDriverFactory.getInstance().getWebDriver().navigate().refresh();
                break;

            case "Microsoft Surface landscape":
                d = new Dimension(1366, 768);
                logger.warn("EMULATED DISPLAY: '" + switchgenre + "' RESOLUTION (CSSPX):" + d.toString());
                WebDriverFactory.getInstance().getWebDriver().manage().window().setSize(d);
                WebDriverFactory.getInstance().getWebDriver().navigate().refresh();
                break;

            case "Microsoft Surface Pro":
                d = new Dimension(720, 1280);
                logger.warn("EMULATED DISPLAY: '" + switchgenre + "' RESOLUTION (CSSPX):" + d.toString());
                WebDriverFactory.getInstance().getWebDriver().manage().window().setSize(d);
                WebDriverFactory.getInstance().getWebDriver().navigate().refresh();
                break;

            case "Microsoft Surface Pro landscape":
                d = new Dimension(1280, 720);
                logger.warn("EMULATED DISPLAY: '" + switchgenre + "' RESOLUTION (CSSPX):" + d.toString());
                WebDriverFactory.getInstance().getWebDriver().manage().window().setSize(d);
                WebDriverFactory.getInstance().getWebDriver().navigate().refresh();
                break;

            case "Microsoft Surface Pro 2":
                d = new Dimension(720, 1280);
                logger.warn("EMULATED DISPLAY: '" + switchgenre + "' RESOLUTION (CSSPX):" + d.toString());
                WebDriverFactory.getInstance().getWebDriver().manage().window().setSize(d);
                WebDriverFactory.getInstance().getWebDriver().navigate().refresh();
                break;

            case "Microsoft Surface Pro 2 landscape":
                d = new Dimension(1280, 720);
                logger.warn("EMULATED DISPLAY: '" + switchgenre + "' RESOLUTION (CSSPX):" + d.toString());
                WebDriverFactory.getInstance().getWebDriver().manage().window().setSize(d);
                WebDriverFactory.getInstance().getWebDriver().navigate().refresh();
                break;

            case "Microsoft Surface Pro 3":
                d = new Dimension(1024, 1440);
                logger.warn("EMULATED DISPLAY: '" + switchgenre + "' RESOLUTION (CSSPX):" + d.toString());
                WebDriverFactory.getInstance().getWebDriver().manage().window().setSize(d);
                WebDriverFactory.getInstance().getWebDriver().navigate().refresh();
                break;

            case "Microsoft Surface Pro 3 landscape":
                d = new Dimension(1440, 1024);
                logger.warn("EMULATED DISPLAY: '" + switchgenre + "' RESOLUTION (CSSPX):" + d.toString());
                WebDriverFactory.getInstance().getWebDriver().manage().window().setSize(d);
                WebDriverFactory.getInstance().getWebDriver().navigate().refresh();
                break;

            case "Motorola Nexus 6":
                d = new Dimension(412, 690);
                logger.warn("EMULATED DISPLAY: '" + switchgenre + "' RESOLUTION (CSSPX):" + d.toString());
                WebDriverFactory.getInstance().getWebDriver().manage().window().setSize(d);
                WebDriverFactory.getInstance().getWebDriver().navigate().refresh();
                break;

            case "Motorola Nexus 6 landscape":
                d = new Dimension(690, 412);
                logger.warn("EMULATED DISPLAY: '" + switchgenre + "' RESOLUTION (CSSPX):" + d.toString());
                WebDriverFactory.getInstance().getWebDriver().manage().window().setSize(d);
                WebDriverFactory.getInstance().getWebDriver().navigate().refresh();
                break;

            case "Samsung Galaxy Nexus":
                d = new Dimension(360, 600);
                logger.warn("EMULATED DISPLAY: '" + switchgenre + "' RESOLUTION (CSSPX):" + d.toString());
                WebDriverFactory.getInstance().getWebDriver().manage().window().setSize(d);
                WebDriverFactory.getInstance().getWebDriver().navigate().refresh();
                break;

            case "Samsung Galaxy Nexus landscape":
                d = new Dimension(600, 360);
                logger.warn("EMULATED DISPLAY: '" + switchgenre + "' RESOLUTION (CSSPX):" + d.toString());
                WebDriverFactory.getInstance().getWebDriver().manage().window().setSize(d);
                WebDriverFactory.getInstance().getWebDriver().navigate().refresh();
                break;

            case "Samsung Galaxy Note":
                d = new Dimension(400, 640);
                logger.warn("EMULATED DISPLAY: '" + switchgenre + "' RESOLUTION (CSSPX):" + d.toString());
                WebDriverFactory.getInstance().getWebDriver().manage().window().setSize(d);
                WebDriverFactory.getInstance().getWebDriver().navigate().refresh();
                break;

            case "Samsung Galaxy Note landscape":
                d = new Dimension(640, 400);
                logger.warn("EMULATED DISPLAY: '" + switchgenre + "' RESOLUTION (CSSPX):" + d.toString());
                WebDriverFactory.getInstance().getWebDriver().manage().window().setSize(d);
                WebDriverFactory.getInstance().getWebDriver().navigate().refresh();
                break;

            case "Samsung Galaxy Note 1, 2, 3, 4":
                d = new Dimension(360, 640);
                logger.warn("EMULATED DISPLAY: '" + switchgenre + "' RESOLUTION (CSSPX):" + d.toString());
                WebDriverFactory.getInstance().getWebDriver().manage().window().setSize(d);
                WebDriverFactory.getInstance().getWebDriver().navigate().refresh();
                break;

            case "Samsung Galaxy Note 1, 2, 3, 4 landscape":
                d = new Dimension(640, 360);
                logger.warn("EMULATED DISPLAY: '" + switchgenre + "' RESOLUTION (CSSPX):" + d.toString());
                WebDriverFactory.getInstance().getWebDriver().manage().window().setSize(d);
                WebDriverFactory.getInstance().getWebDriver().navigate().refresh();
                break;

            case "Samsung Galaxy S1, 2":
                d = new Dimension(320, 533);
                logger.warn("EMULATED DISPLAY: '" + switchgenre + "' RESOLUTION (CSSPX):" + d.toString());
                WebDriverFactory.getInstance().getWebDriver().manage().window().setSize(d);
                WebDriverFactory.getInstance().getWebDriver().navigate().refresh();
                break;

            case "Samsung Galaxy S1, 2 landscape":
                d = new Dimension(533, 320);
                logger.warn("EMULATED DISPLAY: '" + switchgenre + "' RESOLUTION (CSSPX):" + d.toString());
                WebDriverFactory.getInstance().getWebDriver().manage().window().setSize(d);
                WebDriverFactory.getInstance().getWebDriver().navigate().refresh();
                break;

            case "Samsung Galaxy S3":
                d = new Dimension(360, 640);
                logger.warn("EMULATED DISPLAY: '" + switchgenre + "' RESOLUTION (CSSPX):" + d.toString());
                WebDriverFactory.getInstance().getWebDriver().manage().window().setSize(d);
                WebDriverFactory.getInstance().getWebDriver().navigate().refresh();
                break;

            case "Samsung Galaxy S3 landscape":
                d = new Dimension(640, 360);
                logger.warn("EMULATED DISPLAY: '" + switchgenre + "' RESOLUTION (CSSPX):" + d.toString());
                WebDriverFactory.getInstance().getWebDriver().manage().window().setSize(d);
                WebDriverFactory.getInstance().getWebDriver().navigate().refresh();
                break;

            case "Samsung Galaxy S3 Mini":
                d = new Dimension(320, 533);
                logger.warn("EMULATED DISPLAY: '" + switchgenre + "' RESOLUTION (CSSPX):" + d.toString());
                WebDriverFactory.getInstance().getWebDriver().manage().window().setSize(d);
                WebDriverFactory.getInstance().getWebDriver().navigate().refresh();
                break;

            case "Samsung Galaxy S3 Mini landscape":
                d = new Dimension(533, 320);
                logger.warn("EMULATED DISPLAY: '" + switchgenre + "' RESOLUTION (CSSPX):" + d.toString());
                WebDriverFactory.getInstance().getWebDriver().manage().window().setSize(d);
                WebDriverFactory.getInstance().getWebDriver().navigate().refresh();
                break;

            case "Samsung Galaxy S4, 5 Mini":
                d = new Dimension(360, 640);
                logger.warn("EMULATED DISPLAY: '" + switchgenre + "' RESOLUTION (CSSPX):" + d.toString());
                WebDriverFactory.getInstance().getWebDriver().manage().window().setSize(d);
                WebDriverFactory.getInstance().getWebDriver().navigate().refresh();
                break;

            case "Samsung Galaxy S4, 5 Mini landscape":
                d = new Dimension(640, 360);
                logger.warn("EMULATED DISPLAY: '" + switchgenre + "' RESOLUTION (CSSPX):" + d.toString());
                WebDriverFactory.getInstance().getWebDriver().manage().window().setSize(d);
                WebDriverFactory.getInstance().getWebDriver().navigate().refresh();
                break;

            case "Samsung Galaxy Tab 8.9":
                d = new Dimension(800, 1280);
                logger.warn("EMULATED DISPLAY: '" + switchgenre + "' RESOLUTION (CSSPX):" + d.toString());
                WebDriverFactory.getInstance().getWebDriver().manage().window().setSize(d);
                WebDriverFactory.getInstance().getWebDriver().navigate().refresh();
                break;

            case "Samsung Galaxy Tab 8.9 landscape":
                d = new Dimension(1280, 800);
                logger.warn("EMULATED DISPLAY: '" + switchgenre + "' RESOLUTION (CSSPX):" + d.toString());
                WebDriverFactory.getInstance().getWebDriver().manage().window().setSize(d);
                WebDriverFactory.getInstance().getWebDriver().navigate().refresh();
                break;

            case "Samsung Galaxy Tab 2 7.0":
                d = new Dimension(600, 1024);
                logger.warn("EMULATED DISPLAY: '" + switchgenre + "' RESOLUTION (CSSPX):" + d.toString());
                WebDriverFactory.getInstance().getWebDriver().manage().window().setSize(d);
                WebDriverFactory.getInstance().getWebDriver().navigate().refresh();
                break;

            case "Samsung Galaxy Tab 2 7.0 landscape":
                d = new Dimension(1024, 600);
                logger.warn("EMULATED DISPLAY: '" + switchgenre + "' RESOLUTION (CSSPX):" + d.toString());
                WebDriverFactory.getInstance().getWebDriver().manage().window().setSize(d);
                WebDriverFactory.getInstance().getWebDriver().navigate().refresh();
                break;

            case "Samsung Galaxy Tab 2,3 10":
                d = new Dimension(800, 1280);
                logger.warn("EMULATED DISPLAY: '" + switchgenre + "' RESOLUTION (CSSPX):" + d.toString());
                WebDriverFactory.getInstance().getWebDriver().manage().window().setSize(d);
                WebDriverFactory.getInstance().getWebDriver().navigate().refresh();
                break;

            case "Samsung Galaxy Tab 2,3 10 landscape":
                d = new Dimension(1280, 800);
                logger.warn("EMULATED DISPLAY: '" + switchgenre + "' RESOLUTION (CSSPX):" + d.toString());
                WebDriverFactory.getInstance().getWebDriver().manage().window().setSize(d);
                WebDriverFactory.getInstance().getWebDriver().navigate().refresh();
                break;

            case "Samsung Nexus 10":
                d = new Dimension(800, 1280);
                logger.warn("EMULATED DISPLAY: '" + switchgenre + "' RESOLUTION (CSSPX):" + d.toString());
                WebDriverFactory.getInstance().getWebDriver().manage().window().setSize(d);
                WebDriverFactory.getInstance().getWebDriver().navigate().refresh();
                break;

            case "Samsung Nexus 10 landscape":
                d = new Dimension(1280, 800);
                logger.warn("EMULATED DISPLAY: '" + switchgenre + "' RESOLUTION (CSSPX):" + d.toString());
                WebDriverFactory.getInstance().getWebDriver().manage().window().setSize(d);
                WebDriverFactory.getInstance().getWebDriver().navigate().refresh();
                break;

            case "Sony Xperia P, S, Z":
                d = new Dimension(360, 640);
                logger.warn("EMULATED DISPLAY: '" + switchgenre + "' RESOLUTION (CSSPX):" + d.toString());
                WebDriverFactory.getInstance().getWebDriver().manage().window().setSize(d);
                WebDriverFactory.getInstance().getWebDriver().navigate().refresh();
                break;

            case "Sony Xperia P, S, Z landscape":
                d = new Dimension(640, 360);
                logger.warn("EMULATED DISPLAY: '" + switchgenre + "' RESOLUTION (CSSPX):" + d.toString());
                WebDriverFactory.getInstance().getWebDriver().manage().window().setSize(d);
                WebDriverFactory.getInstance().getWebDriver().navigate().refresh();
                break;

            case "Sony Xperia Z3":
                d = new Dimension(360, 598);
                logger.warn("EMULATED DISPLAY: '" + switchgenre + "' RESOLUTION (CSSPX):" + d.toString());
                WebDriverFactory.getInstance().getWebDriver().manage().window().setSize(d);
                WebDriverFactory.getInstance().getWebDriver().navigate().refresh();
                break;

            case "Sony Xperia Z3 landscape":
                d = new Dimension(598, 360);
                logger.warn("EMULATED DISPLAY: '" + switchgenre + "' RESOLUTION (CSSPX):" + d.toString());
                WebDriverFactory.getInstance().getWebDriver().manage().window().setSize(d);
                WebDriverFactory.getInstance().getWebDriver().navigate().refresh();
                break;

            case "Xiaomi Mi 3, 4":
                d = new Dimension(360, 640);
                logger.warn("EMULATED DISPLAY: '" + switchgenre + "' RESOLUTION (CSSPX):" + d.toString());
                WebDriverFactory.getInstance().getWebDriver().manage().window().setSize(d);
                WebDriverFactory.getInstance().getWebDriver().navigate().refresh();
                break;

            case "Xiaomi Mi 3, 4 landscape":
                d = new Dimension(640, 360);
                logger.warn("EMULATED DISPLAY: '" + switchgenre + "' RESOLUTION (CSSPX):" + d.toString());
                WebDriverFactory.getInstance().getWebDriver().manage().window().setSize(d);
                WebDriverFactory.getInstance().getWebDriver().navigate().refresh();
                break;


            default:
                Assert.fail("Your device type has not been found");

        }

    }
}
