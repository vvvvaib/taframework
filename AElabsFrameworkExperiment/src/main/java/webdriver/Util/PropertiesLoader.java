package webdriver.Util;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.ClassPathResource;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;


public class PropertiesLoader {
    private static Logger logger = LoggerFactory.getLogger(PropertiesLoader.class);

    private static PropertiesLoader _instance;

    private Properties properties;


    public PropertiesLoader() {
        load();
    }

    public static PropertiesLoader getInstance() {
        if (_instance == null) {
            _instance = new PropertiesLoader();
        }
        return _instance;
    }

    public String getProperty(String key) {
        String value = properties.getProperty(key);
        if (StringUtils.isEmpty(value)) {
            throw new RuntimeException("De waarde voor " + key + " is leeg of afwezig in het properties-bestand.");
        }
        return value;
    }

    private void load() {
        if (properties == null) {

            try {
                if (System.getProperty("development.properties") == null) {
                    System.setProperty("development.properties","Appium.properties");
                            logger.warn("Gebruikte Properties gevonden");
                }
                loadProperties();
            } catch (IOException e) {
                logger.error("Het properties-bestand kon niet geladen worden.", e);
            }

        }
    }

    private void loadProperties() throws IOException {
        InputStream is = null;
        try {
            logger.info("Loading " + System.getProperty("development.properties"));
            properties = new Properties();
            ClassPathResource resource = new ClassPathResource(System.getProperty("development.properties"));
            is = resource.getInputStream();
            properties.load(is);
        } finally {
            if (is != null) {
                is.close();
            }
        }
    }

 }



