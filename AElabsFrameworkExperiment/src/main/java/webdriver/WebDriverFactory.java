package webdriver;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.rules.TestName;
import org.junit.rules.TestRule;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.opera.OperaDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import io.github.bonigarcia.wdm.ChromeDriverManager;
import io.github.bonigarcia.wdm.EdgeDriverManager;
import io.github.bonigarcia.wdm.InternetExplorerDriverManager;
import io.github.bonigarcia.wdm.OperaDriverManager;
import org.junit.Assert;

public class WebDriverFactory {
	private String type = System.getProperty("browser", "chrome");
	public TestName testName = new TestName();
	private static WebDriverFactory instance = new WebDriverFactory();

	public static WebDriverFactory getInstance() {
		return instance;
	}

	public WebDriver getWebDriver() // call this method to get the driver object
									// and launch the browser
	{
		return driver.get();
	}

	public void removeDriver() // Quits the driver and closes the browser
	{
		driver.get().close();
		driver.remove();
	}

	@Rule
	public TestRule watcher = new TestWatcher() {

		@Override
		protected void starting(Description description) {
			System.out.println("**********************************************************************");
			System.out.println("Start test");
			System.out.println("**********************************************************************");
		}

		@Override
		protected void finished(Description description) {
			System.out.println("**********************************************************************");
			System.out.println("Finished test : " + description.getClassName() + "." + testName.getMethodName());
			System.out.println("**********************************************************************");
		}
	};

	@Before
	public void setUp() throws Exception {
		WebDriverFactory.getInstance().getWebDriver().manage().deleteAllCookies();
		if (type.equalsIgnoreCase("Iexplore")) {
			Thread.sleep(5000);
		}
		WebDriverFactory.getInstance().getWebDriver().manage().window().maximize();
		//String filePath = "file://" + System.getProperty("user.dir") + "/examplesites/index.html";
	//	WebDriverFactory.getInstance().getWebDriver().navigate().to(filePath);
	}

	ThreadLocal<WebDriver> driver = new ThreadLocal<WebDriver>() {

		@Override
		protected WebDriver initialValue() {
			// Setup for the right browser to run your test with : Can be used
			// with -browser = 'NameOfYourBrowser'
			if (type.equals("Firefox")) {
				return new FirefoxDriver();
			} else if (type.equalsIgnoreCase("Iexplore")) {
				InternetExplorerDriverManager.getInstance().setup("2.48");
				DesiredCapabilities cap = DesiredCapabilities.internetExplorer();
				cap.setCapability(InternetExplorerDriver.IE_ENSURE_CLEAN_SESSION, true);

				return new InternetExplorerDriver(cap);

			} else if (type.equalsIgnoreCase("Opera")) {
				OperaDriverManager.getInstance().setup();
				return new OperaDriver();
			} // Opera = broken

			else if (type.equalsIgnoreCase("Edge")) {
				EdgeDriverManager.getInstance().setup();
				return new EdgeDriver();
			}

			else {
				ChromeDriverManager.getInstance().setup();
				return new ChromeDriver();
			}
		}
	};

	@After
	public void tearDown() throws Exception {
		WebDriverFactory.getInstance().removeDriver();
	}
	
	public void click (WebElement e) throws WebDriverException
	{
		e.click();
	}
	
	public String getText (WebElement e) throws WebDriverException
	{
		return e.getText();
	}
	
	public void sendText (WebElement e, String keys) throws WebDriverException
	{
		e.sendKeys(keys);
	}
	
	public void launch (String url) throws WebDriverException
	{
		WebDriverFactory.getInstance().getWebDriver().navigate().to(url);
	}
	
	public void hover(WebElement e) throws WebDriverException
	{
		Actions action = new Actions(WebDriverFactory.getInstance().getWebDriver());
        action.moveToElement(e).build().perform();
	}
	
	public void checkText(WebElement e, String textToCheck) throws WebDriverException
	{
		Assert.assertTrue(e.getText().contains(textToCheck));
	}
	
	public void checkElementPresent(WebElement e) throws WebDriverException
	{
		Assert.assertTrue(e.isEnabled());
	}
	
	
}
