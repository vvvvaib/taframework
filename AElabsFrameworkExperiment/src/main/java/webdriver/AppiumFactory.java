package webdriver;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.remote.MobileCapabilityType;
import org.junit.After;
import org.junit.Before;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import webdriver.Util.PropertiesLoader;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * This class is designed for running mobile test with appium. Because appium need some builds to get started.
 * this file will build up the things that stay the same in every test.
 * The variables of this file will come out of a property file where we put everything.
 */
public class AppiumFactory {

    private static AppiumDriver driver;
    File appDir = new File("/var/lib/jenkins/jobs/Deploy_app/workspace/");
    File app = new File(appDir, "Sporza%20Voetbal_1.10_apk-dl.com.apk");

    PropertiesLoader p = new PropertiesLoader();

    private static AppiumFactory instance = new AppiumFactory();

    public static AppiumFactory getInstance() {
        return instance;
    }

    @Before
    public void before() throws MalformedURLException{

        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setCapability("appium-version", p.getProperty("appiumversion"));
        capabilities.setCapability("app", app.getAbsolutePath());

        if (p.getProperty("PlatformName").equals("Android")) {
            capabilities.setCapability(MobileCapabilityType.PLATFORM_VERSION, p.getProperty("AndroidVersion"));
            capabilities.setCapability(MobileCapabilityType.PLATFORM_NAME, p.getProperty("PlatformName"));
            capabilities.setCapability(MobileCapabilityType.DEVICE_NAME, p.getProperty("DeviceName"));
            setCorrectURL(capabilities);
        }
        else if(p.getProperty("PlatformName").equals("iOS")){
            capabilities.setCapability(MobileCapabilityType.PLATFORM_VERSION, p.getProperty("AndroidVersion"));
            capabilities.setCapability(MobileCapabilityType.PLATFORM_NAME, p.getProperty("PlatformName"));
            capabilities.setCapability(MobileCapabilityType.DEVICE_NAME, p.getProperty("DeviceName"));
            setCorrectURL(capabilities);
        }

    }

    public void setCorrectURL(DesiredCapabilities capabilities) throws MalformedURLException {

        if (System.getProperty("os.name").equals("Linux")) {
            driver = new AppiumDriver(new URL("http://0.0.0.0:4723/wd/hub"), capabilities) {

               // @Override
                public WebElement scrollTo(String s) {
                    return null;
                }

              //  @Override
                public WebElement scrollToExact(String s) {
                    return null;
                }};}

        //This is the correct url for Windows & Mac Users
        else {
            driver = new AppiumDriver(new URL("http://127.0.0.1:4723/wd/hub"), capabilities) {
              //  @Override
                public WebElement scrollTo(String s) {
                    return null;
                }

               // @Override
                public WebElement scrollToExact(String s) {
                    return null;
                }};}}

    public WebDriver getAndroidriver() // call this method to get the driver object and launch the browser
    {
        return driver;
    }
    @After
    public void after(){
        driver.quit();
    }

}
