package webdriver;

import org.apache.bcel.generic.RETURN;
import org.junit.Assert;
import org.openqa.selenium.Keys;
import org.sikuli.script.FindFailed;
import org.sikuli.script.Finder;
import org.sikuli.script.Screen;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;

/**
 * This class can be used to do everything with sikuli, this is a tool that can look up items and click items by a screen shot
 * all you need to do is take a screen shot of the thing you want to search and sikuli does the rest for you.
 */
public class VisualTestingFactory {

    Logger logger = LoggerFactory.getLogger(VisualTestingFactory.class);
    Screen screen = new Screen();
    public static final String IMAGES_URL = System.getProperty("user.dir")+"/src/main/resources/Images/";

    public String buildPathToFile(String filename){
        System.out.println(Paths.get(IMAGES_URL +filename));
        return Paths.get(IMAGES_URL + filename).toString();
    }

    public void clickOnScreen(String img){
        try {
            screen.click(buildPathToFile(img));
        } catch (FindFailed findFailed) {
            Assert.fail("The image couldn't be found");
            findFailed.printStackTrace();
        }
    }
    public void rightClickOnScreen(String img) {
        try {
            screen.rightClick(buildPathToFile(img));
        } catch (FindFailed findFailed) {
            Assert.fail("The image couldn't be found");
            findFailed.printStackTrace();
        }
    }

    public void doubleClickOnScreen(String img) {
        try {
            screen.doubleClick(buildPathToFile(img));
        } catch (FindFailed findFailed) {
            Assert.fail("The image couldn't be found");
            findFailed.printStackTrace();
        }
    }
    //This will make a screenshot of the current page you are in.
    public void takeAScreenShot(String fileName) {
        logger.info("Take a screenshot from the current view");
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        Rectangle screenRectangle = new Rectangle(screenSize);
        Robot robot = null;
        try {
            robot = new Robot();
        } catch (AWTException e) {
            e.printStackTrace();
        }
        BufferedImage image = robot.createScreenCapture(screenRectangle);
        try {
            ImageIO.write(image,"png", new File(IMAGES_URL+fileName+".png"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void compareScreens(String fileName, String compare){
        logger.info("Comparing Screenshot with expected view");
        try {
            Finder f = new Finder(IMAGES_URL+fileName);
            f.find(IMAGES_URL+compare);
            if(f.hasNext()) {
               logger.info("Images are identical");
                            }else{

                    Assert.fail("Images not are not identical");
                }
                f.destroy();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    public void typeWordInScreen(String word){
        logger.info("Typ the word you want" +word);
        screen.type(word);
        logger.info("Typ an Return");
        screen.click();
    }
}



