/******************************************************************************
Developed by: Vaibhav
*******************************************************************************/

package be.ae.ln.demo.www.ORLayer;

import org.junit.Test;
import org.openqa.selenium.interactions.Actions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import paginas.be.ln.www.CommonControls;
import paginas.be.ln.www.HomePage;
import paginas.be.ln.www.JobsPage;
import paginas.be.ln.www.LoginPage;
import paginas.be.ln.www.ProfilePage;
import paginas.be.ln.www.SignOutPage;
import webdriver.WebDriverFactory;

public class testDemo1 extends WebDriverFactory {

	private String urlln = "https://www.linkedin.com";
	Logger logger = LoggerFactory.getLogger(testDemo1.class);
	WebDriverFactory wdf = new WebDriverFactory();
	@Test
	public void loginTest() {
		
		wdf.launch(urlln);
		wdf.sendText(new LoginPage().emailID, "aeqacap@gmail.com");
		wdf.sendText(new LoginPage().passwordField, ""); 	//password removed by Vaibhav
		wdf.click(new LoginPage().signInButton);
		wdf.checkText(new CommonControls().homeLink, "Home");

	}

	@Test
	public void navigationTest() {
		wdf.launch(urlln);
		wdf.sendText(new LoginPage().emailID, "aeqacap@gmail.com");
		wdf.sendText(new LoginPage().passwordField, "");		//password removed by Vaibhav
		wdf.click(new LoginPage().signInButton);
		wdf.click(new CommonControls().profilesLink);
		wdf.checkText(new ProfilePage().profileStrengthText,"Profile Strength");
		wdf.click(new CommonControls().jobsLink);
		wdf.checkText(new JobsPage().jobsHomeText,"Jobs");
	}

	@Test
	public void logoutTest() {
		wdf.launch(urlln);
		wdf.sendText(new LoginPage().emailID, "aeqacap@gmail.com");
		wdf.sendText(new LoginPage().passwordField, "");		//password removed by Vaibhav
		wdf.click(new LoginPage().signInButton);
		wdf.hover(new CommonControls().accountsAndSettingsImage);
		wdf.click(new CommonControls().signOutElement);
		wdf.checkText(new SignOutPage().youHaveSignedOutText,"You have signed out");
	}

}
