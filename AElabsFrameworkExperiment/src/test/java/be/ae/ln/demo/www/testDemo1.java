/******************************************************************************
Developed by: Vaibhav
*******************************************************************************/

package be.ae.ln.demo.www;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import paginas.be.ln.www.HomePage;
import paginas.be.ln.www.LoginPage;
import paginas.be.ln.www.ProfilePage;
import webdriver.WebDriverFactory;

public class testDemo1 extends WebDriverFactory {

	private String urlln = "https://www.linkedin.com";

	Logger logger = LoggerFactory.getLogger(testDemo1.class);

	@Test
	public void loginTest() {
		WebDriverFactory.getInstance().getWebDriver().navigate().to(urlln);
		new LoginPage().enteremailID("aeqacap@gmail.com")
		.enterpassword("********")  //Password is removed(Vaibhav :31/5/2016)
		.klikOpLoginTab()
		;
		logger.info("USER LOGGED IN SUCCESSFULLY");

	}

	@Test
	public void navigationTest() {
		WebDriverFactory.getInstance().getWebDriver().navigate().to(urlln);
		new LoginPage().loginPositive();

		new HomePage().klikOpProfileLink();
		logger.info("USER ON PROFILE PAGE");
		new ProfilePage().klikOpJobsLink();
		logger.info("USER ON JOBS PAGE");

	}

	@Test
	public void logoutTest() {
		WebDriverFactory.getInstance().getWebDriver().navigate().to(urlln);
		new LoginPage().loginPositive();
		new HomePage().klikOpSignOut();

	}

}
